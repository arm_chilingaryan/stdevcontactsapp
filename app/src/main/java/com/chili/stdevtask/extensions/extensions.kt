package com.chili.stdevtask.extensions

import android.content.Context
import android.text.Editable
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.chili.stdevtask.App
import com.chili.stdevtask.utils.TextChecker
import com.google.android.material.textfield.TextInputLayout

fun showToast(message: String?) {

    val s: String = if (message.isNullOrEmpty()) "empty message" else message
    Toast.makeText(App.instance, s, Toast.LENGTH_LONG).show()
}

fun TextInputLayout.addTextChecker() {
    editText?.addTextChangedListener(object : TextChecker() {
        override fun afterTextChanged(s: Editable?) {
            error = null
            isErrorEnabled = false
        }
    })
}

fun View.hideKeyboard() {

    val imm = App.instance.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm?.let {
        it.hideSoftInputFromWindow(windowToken, 0)
    }
}
