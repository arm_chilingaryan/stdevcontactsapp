package com.chili.stdevtask.model.event_bus;

public class UpdateContacts {
 
    public final boolean popFragment;
 
    public UpdateContacts(boolean popFragment) {
        this.popFragment = popFragment;
    }
}