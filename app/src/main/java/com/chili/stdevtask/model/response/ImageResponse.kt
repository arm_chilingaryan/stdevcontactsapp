package com.chili.stdevtask.model.response

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ImageResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("uploadid")
	val uploadid: String? = null,

	@field:SerializedName("ids")
	val ids: List<String?>? = null
)