package com.chili.stdevtask.model.response

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.chili.stdevtask.utils.DataConverter
import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Entity(tableName = "contacts")
@TypeConverters(DataConverter::class)
@Generated("com.robohorse.robopojogenerator")

data class Contact(

    @field:SerializedName("firstName")
    val firstName: String? = null,

    @field:SerializedName("lastName")
    val lastName: String? = null,

    @field:SerializedName("images")
    val images: List<String?>? = listOf(),

    @field:SerializedName("notes")
    val notes: String? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @PrimaryKey(autoGenerate = false)
    @field:SerializedName("_id")
    @NonNull
    val id: String,

    @field:SerializedName("email")
    val email: String? = null

) {

    fun getFullName(): String {
        return firstName.plus(" ").plus(lastName)
    }
}