package com.chili.stdevtask.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

data class NewContactRequest(

	@field:SerializedName("firstName")
	var firstName: String? = null,

	@field:SerializedName("lastName")
	var lastName: String? = null,

	@field:SerializedName("images")
	var images: List<String?>? = null,

	@field:SerializedName("notes")
	var notes: String? = null,

	@field:SerializedName("phone")
	var phone: String? = null,

	@field:SerializedName("email")
	var email: String? = null
)