package com.chili.stdevtask

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
        Fresco.initialize(instance)

    }

    companion object {
        lateinit var instance: App
            private set
    }
}