package com.chili.stdevtask.utils

import android.text.TextWatcher

// Abstract class to use only after text change method
abstract class TextChecker : TextWatcher {
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }
}