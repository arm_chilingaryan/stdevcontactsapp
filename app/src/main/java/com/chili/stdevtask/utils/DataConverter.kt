package com.chili.stdevtask.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DataConverter {

    @TypeConverter // note this annotation
    fun toJson(optionValues: List<String>?): String? {
        if (optionValues.isNullOrEmpty()) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {

        }.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun fromJsonToList(optionValuesString: String?): List<String>? {
        if (optionValuesString.isNullOrEmpty()) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {

        }.type
        return gson.fromJson(optionValuesString, type)
    }

}
 