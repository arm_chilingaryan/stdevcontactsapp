package com.chili.stdevtask.data.db.contacts_db

import com.chili.stdevtask.data.db.app_db.AppDatabaseProvider


object ContactLocalSourceProvider {

    fun provideContactLocalSource(): ContactsLocalSource {
        return ContactsLocalSource(AppDatabaseProvider.appDatabase.contactsDao())
    }

}