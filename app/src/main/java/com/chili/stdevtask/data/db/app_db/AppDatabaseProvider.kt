package com.chili.stdevtask.data.db.app_db

import com.chili.stdevtask.App


object AppDatabaseProvider {
    // single instance
    val appDatabase: AppDatabase by lazy {
        AppDatabase(App.instance)
    }
}