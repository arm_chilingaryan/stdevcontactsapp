package com.chili.stdevtask.data.remote

import com.chili.stdevtask.model.NewContactRequest
import com.chili.stdevtask.model.response.Contact
import io.reactivex.Observable

class ContactsRemoteSource(private val apiService: ContactsApi) : ContactsApi {
    override fun patchContact(id: String, contactRequest: NewContactRequest): Observable<Contact> {
        return apiService.patchContact(id, contactRequest)
    }

    override fun createContact(contactRequest: NewContactRequest): Observable<Contact> {
        return apiService.createContact(contactRequest)
    }

    override fun getAllContacts(): Observable<List<Contact>> {
        return apiService.getAllContacts()
    }

}