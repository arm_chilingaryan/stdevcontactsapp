package com.chili.stdevtask.data.remote

import com.chili.stdevtask.model.NewContactRequest
import com.chili.stdevtask.model.response.Contact
import com.chili.stdevtask.model.response.ImageResponse
import io.reactivex.Observable
import okhttp3.MultipartBody

class MediaRemoteSource(private val apiService: MediaApi) : MediaApi {
    override fun uploadImage(filePart: MultipartBody.Part): Observable<ImageResponse> {
        return apiService.uploadImage(filePart)
    }

}