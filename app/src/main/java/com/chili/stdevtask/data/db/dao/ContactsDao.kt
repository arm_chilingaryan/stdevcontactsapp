package com.chili.stdevtask.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chili.stdevtask.model.response.Contact
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface ContactsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(contactItem: Contact)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertConstacts(contactItems: List<Contact>): List<Long>

    @Query("select * from contacts")
    fun getContacts(): Observable<List<Contact>>

    @Query("select * from contacts where id = :id")
    fun getContactById(id: String): Observable<Contact>
}