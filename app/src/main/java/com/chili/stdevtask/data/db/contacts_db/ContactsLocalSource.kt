package com.chili.stdevtask.data.db.contacts_db

import com.chili.stdevtask.data.db.dao.ContactsDao
import com.chili.stdevtask.model.response.Contact
import io.reactivex.Observable

class ContactsLocalSource(private val contactsDao: ContactsDao) {

    fun getAllContacts(): Observable<List<Contact>> {
        return contactsDao.getContacts()
    }

    fun upsertContacts(contactsList: List<Contact>): List<Long> {
        return contactsDao.upsertConstacts(contactsList)
    }

    fun getContactById(id: String): Observable<Contact> {
        return contactsDao.getContactById(id)
    }
}