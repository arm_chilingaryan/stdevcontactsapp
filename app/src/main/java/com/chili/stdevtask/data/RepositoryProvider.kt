package com.chili.stdevtask.data

import com.chili.stdevtask.data.db.contacts_db.ContactLocalSourceProvider
import com.chili.stdevtask.data.remote.RemoteSourceProvider

object RepositoryProvider {
    private val remoteSource by lazy {
        RemoteSourceProvider.provideRemoteSource()
    }

    private val contactsLocalSource by lazy {
        ContactLocalSourceProvider.provideContactLocalSource()
    }

    fun getRepository(): Repository {
        return Repository(remoteSource, contactsLocalSource)
    }
}