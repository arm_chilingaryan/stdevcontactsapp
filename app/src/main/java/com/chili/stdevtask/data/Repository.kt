package com.chili.stdevtask.data

import com.chili.stdevtask.data.db.contacts_db.ContactsLocalSource
import com.chili.stdevtask.data.remote.RemoteSource
import com.chili.stdevtask.model.NewContactRequest
import com.chili.stdevtask.model.response.Contact
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class Repository(private val remoteSource: RemoteSource, private var contactsLocalSource: ContactsLocalSource) {

    //region Create contact part
    fun createContact(contactObj: NewContactRequest, file: File?): Observable<Contact> {
        val observable = if (file == null) {
            createContactRequest(contactObj)
        } else {
            createContactRequestWithImage(contactObj, file)
        }
        return observable
    }

    private fun createContactRequest(contactObj: NewContactRequest): Observable<Contact> {

        return remoteSource.apiService.createContact(contactObj)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    private fun createContactRequestWithImage(contactObj: NewContactRequest, file: File): Observable<Contact> {
        val filePart =
            MultipartBody.Part.createFormData("file", file.name, RequestBody.create(MediaType.parse("image/*"), file))

        return remoteSource.mediaApi.uploadImage(filePart)
            .subscribeOn(Schedulers.io())
            .flatMap {
                contactObj.images = it.ids
                return@flatMap remoteSource.apiService.createContact(contactObj)
            }
            .observeOn(AndroidSchedulers.mainThread())
    }
    //endregion

    //
    fun getContacts(): Observable<List<Contact>> {
        return remoteSource.apiService.getAllContacts()
            .subscribeOn(Schedulers.io())
            .flatMap {
                return@flatMap Observable.fromCallable { contactsLocalSource.upsertContacts(it) }
            }
            .flatMap {
                return@flatMap contactsLocalSource.getAllContacts()
            }
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getContactById(id: String): Observable<Contact> {
        return contactsLocalSource.getContactById(id)
    }

    fun patchContact(id: String, contactObj: NewContactRequest): Observable<Contact> {
       return remoteSource.apiService.patchContact(id, contactObj)
    }

}