package com.chili.stdevtask.data.db.app_db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.chili.stdevtask.data.db.dao.ContactsDao
import com.chili.stdevtask.model.response.Contact
import com.chili.stdevtask.utils.DataConverter


@Database(entities = [Contact::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun contactsDao(): ContactsDao


    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(
            LOCK
        ) {
            instance
                ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "contacts.db")
                .fallbackToDestructiveMigration()
//                .allowMainThreadQueries()
                .build()
    }
}