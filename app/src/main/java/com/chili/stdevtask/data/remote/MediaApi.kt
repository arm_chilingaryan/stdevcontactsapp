package com.chili.stdevtask.data.remote

import com.chili.stdevtask.model.NewContactRequest
import com.chili.stdevtask.model.response.Contact
import com.chili.stdevtask.model.response.ImageResponse
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


interface MediaApi {

    @Multipart
    @POST("media")
    fun uploadImage(@Part filePart: MultipartBody.Part): Observable<ImageResponse>

    companion object {
        const val BASE_URL_MEDIA = "https://stdevtask3-0510.restdb.io/"
        //region Retrofit Client creation
        fun create(): MediaApi {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL_MEDIA)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(createHttpClient())
                    .build()

            return retrofit.create(MediaApi::class.java)

        }

        private fun createHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                    .callTimeout(90, TimeUnit.SECONDS)
                    .addInterceptor(getHeaderInterceptor())
                    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build()

        }

        private fun getHeaderInterceptor(): Interceptor {

            return Interceptor {
                var request = it.request()
                val headers = request.headers().newBuilder()
                        .add("x-apikey", "a5b39dedacbffd95e1421020dae7c8b5ac3cc").build()
                request = request.newBuilder().headers(headers).build()
                it.proceed(request)
            }
        }
        //endregion
    }
}