package com.chili.stdevtask.data.remote


object RemoteSourceProvider {
    // api service created one time
    private val contactsApiService by lazy {
        ContactsApi.create()
    }
    private val mediaApiService by lazy {
        MediaApi.create()
    }

//    fun provideContactRemoteSource(): ContactsRemoteSource {
//        return ContactsRemoteSource(apiService = contactsApiService)
//    }
//
//    fun provideMediaRemoteSource(): MediaRemoteSource {
//        return MediaRemoteSource(apiService = mediaApiService)
//    }

    fun provideRemoteSource(): RemoteSource {
        return RemoteSource(apiService = contactsApiService,mediaApi = mediaApiService)
    }
}