package com.chili.stdevtask.view.contacts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chili.stdevtask.data.Repository
import com.chili.stdevtask.model.response.Contact
import io.reactivex.disposables.CompositeDisposable


class ContactsViewModel(val repository: Repository) : ViewModel() {
    private val contactsLiveData = MutableLiveData<List<Contact>>()
    var compositeDisposable = CompositeDisposable()

    fun getContacts(): LiveData<List<Contact>> {
        val disposable = repository.getContacts()
            .subscribe(
                {
                    contactsLiveData.postValue(it)
                },
                { t: Throwable? ->
                    t?.printStackTrace()
                })

        compositeDisposable.add(disposable)
        return contactsLiveData
    }


    class ContactsViewModelFactory(var repository: Repository) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ContactsViewModel(repository) as T
        }
    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed)
            compositeDisposable.dispose()
        super.onCleared()
    }
}
