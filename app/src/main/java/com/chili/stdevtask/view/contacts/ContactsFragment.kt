package com.chili.stdevtask.view.contacts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chili.stdevtask.R
import com.chili.stdevtask.adapter.ContactClickListener
import com.chili.stdevtask.adapter.ContactsRecyclerAdapter
import com.chili.stdevtask.data.RepositoryProvider
import com.chili.stdevtask.model.event_bus.UpdateContacts
import com.chili.stdevtask.model.response.Contact
import com.chili.stdevtask.view.contact_detail.ContactDetailFragment
import com.chili.stdevtask.view.createContact.CreateContactFragment
import kotlinx.android.synthetic.main.contacts_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class ContactsFragment : Fragment(), ContactClickListener {


    private lateinit var mLayoutManager: LinearLayoutManager
    private var contactsList = arrayListOf<Contact>()
    private lateinit var mAdapter: ContactsRecyclerAdapter

    companion object {
        fun newInstance() = ContactsFragment()
    }

    private lateinit var viewModel: ContactsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.contacts_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolBar()
        initRecyclerView()
        initFab()
    }

    private fun initFab() {
        fab_add_contact.setOnClickListener {
            fragmentManager
                ?.beginTransaction()
                ?.add(R.id.fragment_container, CreateContactFragment.newInstance())
                ?.addToBackStack(null)
                ?.commit()
        }
    }


    private fun initRecyclerView() {
        mLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        contacts_rv.layoutManager = mLayoutManager
        mAdapter = ContactsRecyclerAdapter(contactsList)
        contacts_rv.adapter = mAdapter
        mAdapter.setItemClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(this, ContactsViewModel.ContactsViewModelFactory(RepositoryProvider.getRepository()))
                .get(ContactsViewModel::class.java)
        viewModel.getContacts().observe(this, Observer {
            // update recycler view
            mAdapter.updateList(it)

        })
    }

    override fun onContactClicked(contact: Contact, position: Int) {
        fragmentManager?.beginTransaction()
            ?.add(R.id.fragment_container, ContactDetailFragment.newInstance(contact.id))
            ?.addToBackStack(null)
            ?.commit()
    }


    private fun initToolBar() {
        val activity = activity as AppCompatActivity
        activity.setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.contacts_activity)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener {
            activity.onBackPressed()
        }
        // for showing Back Button
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    //region Event bus , update Contacts event
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: UpdateContacts) {
        viewModel.getContacts()
        if (event.popFragment) {
            activity?.onBackPressed()
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }
    //endregion
}
