package com.chili.stdevtask.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.chili.stdevtask.R
import com.chili.stdevtask.view.contacts.ContactsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, ContactsFragment.newInstance())
                .commit()
        }
    }


}
