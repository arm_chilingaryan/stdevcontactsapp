package com.chili.stdevtask.view.contact_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chili.stdevtask.data.Repository
import com.chili.stdevtask.model.NewContactRequest
import com.chili.stdevtask.model.response.Contact
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ContactDetailViewModel(private var repository: Repository) : ViewModel() {
    private var compositeDisposable = CompositeDisposable()
    private var contactLiveData = MutableLiveData<Contact>()

    fun getContactsById(id: String): LiveData<Contact> {
        val disposable = repository.getContactById(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                contactLiveData.postValue(it)
            }, { t: Throwable? ->
                t?.printStackTrace()
            })
        compositeDisposable.add(disposable)
        return contactLiveData

    }

    fun updateContact(id: String, newContactRequest: NewContactRequest): LiveData<Contact> {
        val disposable = repository.patchContact(id, newContactRequest)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                contactLiveData.postValue(it)
            },
                { t: Throwable? ->
                    t?.printStackTrace()
                })

        compositeDisposable.add(disposable)
        return contactLiveData
    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed)
            compositeDisposable.dispose()
        super.onCleared()
    }

    class ContactDetailViewModelFactory(var repository: Repository) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ContactDetailViewModel(repository) as T
        }
    }
}

