package com.chili.stdevtask.view.createContact

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Patterns
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.chili.stdevtask.BuildConfig
import com.chili.stdevtask.R
import com.chili.stdevtask.data.RepositoryProvider
import com.chili.stdevtask.extensions.addTextChecker
import com.chili.stdevtask.extensions.hideKeyboard
import com.chili.stdevtask.extensions.showToast
import com.chili.stdevtask.model.NewContactRequest
import com.chili.stdevtask.model.event_bus.UpdateContacts
import com.chili.stdevtask.model.response.Contact
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.content_contact_fields.*
import kotlinx.android.synthetic.main.create_contact_fragment.*
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.io.IOException


class CreateContactFragment : Fragment(), View.OnClickListener {

    companion object {
        fun newInstance() = CreateContactFragment()

    }

    private lateinit var viewModel: CreateContactViewModel
    private var photoFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.create_contact_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // disabled by default , enable when edit button clicked
        enableInputLayouts(true)
        initToolBar()
        setTextCheckers()
        userImage.setOnClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
                ViewModelProviders.of(this, CreateContactViewModel.CreateContactViewModelFactory(RepositoryProvider.getRepository()))
                        .get(CreateContactViewModel::class.java)


    }

    //region Options Menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_create_contact, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.done -> {
                createContact()
            }
        }
        return super.onOptionsItemSelected(item)

    }
    //endregion

    private fun createContact() {
        if (checkRequiredFieldsNotEmpty()) {
            view?.hideKeyboard()
            val contactObj = NewContactRequest()
            contactObj.email = emailInputLayout.editText?.text.toString()
            contactObj.phone = phoneInputLayout.editText?.text.toString()
            contactObj.firstName = nameInputLayout.editText?.text.toString()
            contactObj.lastName = surnameInputLayout.editText?.text.toString()
            contactObj.notes = notesInputLayout.editText?.text.toString()
            viewModel.createContact(contactObj, photoFile).observe(this , Observer {
                EventBus.getDefault().post(UpdateContacts(true))
            })
        }

    }

    fun takePicture() {
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION

        val filename = System.currentTimeMillis().toString() + ".jpg"
        photoFile = File(context?.cacheDir, filename)

        if (photoFile==null){
            showToast("Something went wrong")
            return
        }
        if (!photoFile!!.exists())
            try {
                photoFile!!.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        val imageUri = FileProvider.getUriForFile(context!!,
                BuildConfig.APPLICATION_ID + ".fileprovider", photoFile!!)

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        intent.putExtra("outputX", 400)
        intent.putExtra("outputY", 400)

        startActivityForResult(intent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            updateUserImage(photoFile)
        }
    }

    private fun updateUserImage(photoFile: File?) {
        if (photoFile != null) {
            userImage.setImageURI(Uri.fromFile(photoFile), context)
        }
    }

    private fun enableInputLayouts(enable: Boolean) {
        nameInputLayout.editText?.isEnabled = enable
        surnameInputLayout?.isEnabled = enable
        phoneInputLayout?.isEnabled = enable
        emailInputLayout?.isEnabled = enable
        notesInputLayout?.isEnabled = enable
    }

    private fun getImageID(contact: Contact): String? {
        var imageID: String? = null;
        contact.images?.let {
            if (it.isNotEmpty()) {
                imageID = it[0]
            }
        }
        return imageID
    }

    private fun initToolBar() {
        val activity = activity as AppCompatActivity
        activity.setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.create_new_contact)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener {
            activity.onBackPressed()
        }
        // for showing Back Button
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun checkRequiredFieldsNotEmpty(): Boolean {

        if (checkInput(nameInputLayout)
                && checkInput(surnameInputLayout)
                && checkInput(phoneInputLayout)
                && checkEmailValidity(emailInputLayout)
                && checkInput(notesInputLayout)) {
            return true
        }
        return false
    }

    private fun checkInput(inputView: TextInputLayout): Boolean {
        if (inputView.editText?.text.isNullOrEmpty()) {
            inputView.isErrorEnabled = true
            inputView.error = getString(R.string.errorText)
            return false
        }
        return true
    }

    private fun checkEmailValidity(inputView: TextInputLayout): Boolean {
        val text = inputView.editText?.text.toString()
        val isValid = (!TextUtils.isEmpty(text) && Patterns.EMAIL_ADDRESS.matcher(text).matches())

        if (!isValid) {
            inputView.isErrorEnabled = true
            inputView.error = getString(R.string.emailErrorText)
        }
        return isValid
    }

    private fun setTextCheckers() {
        nameInputLayout.addTextChecker()
        surnameInputLayout.addTextChecker()
        phoneInputLayout.addTextChecker()
        emailInputLayout.addTextChecker()
        notesInputLayout.addTextChecker()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.userImage -> {
                takePicture()
            }
        }
    }
}
