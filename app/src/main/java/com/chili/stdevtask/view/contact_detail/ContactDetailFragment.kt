package com.chili.stdevtask.view.contact_detail

import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.chili.stdevtask.R
import com.chili.stdevtask.data.RepositoryProvider
import com.chili.stdevtask.data.remote.ContactsApi
import com.chili.stdevtask.data.remote.ContactsApi.Companion.MEDIA_PATH
import com.chili.stdevtask.data.remote.MediaApi.Companion.BASE_URL_MEDIA
import com.chili.stdevtask.extensions.addTextChecker
import com.chili.stdevtask.extensions.hideKeyboard
import com.chili.stdevtask.model.NewContactRequest
import com.chili.stdevtask.model.event_bus.UpdateContacts
import com.chili.stdevtask.model.response.Contact
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.contact_detail_fragment.*
import kotlinx.android.synthetic.main.content_contact_fields.*
import org.greenrobot.eventbus.EventBus


const val KEY_CONTACT_ID = "key_contact_id"

class ContactDetailFragment : Fragment() {
    private var contactId: String = "-1"
    private lateinit var viewModel: ContactDetailViewModel
    private var isMenuItemChecked = false

    companion object {
        fun newInstance(id: String): ContactDetailFragment {
            val bundle = Bundle();
            bundle.putString(KEY_CONTACT_ID, id)
            val fragment = ContactDetailFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        contactId = arguments?.getString(KEY_CONTACT_ID) ?: "-1"
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.contact_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // disabled by default , enable when edit button clicked
        enableInputLayouts(false)
        initToolBar()
        setTextCheckers()
    }

    private var mContact: Contact? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(
                this,
                ContactDetailViewModel.ContactDetailViewModelFactory(RepositoryProvider.getRepository())
            )
                .get(ContactDetailViewModel::class.java)

        viewModel.getContactsById(contactId).observe(this, Observer {
            mContact = it
            updateUi(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_contact_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.edit -> {
                if (isMenuItemChecked) {
                    if (checkRequiredFieldsNotEmpty()) {
                        view?.hideKeyboard()
                        updateContact()
                        toggleMenuItem(item)
                    }
                } else {
                    enableInputLayouts(!isMenuItemChecked)
                    toggleMenuItem(item)
                }


            }
        }
        return super.onOptionsItemSelected(item)

    }

    private fun toggleMenuItem(item: MenuItem) {
        item.isChecked = !item.isChecked
        isMenuItemChecked = item.isChecked
        // StateSelector doesn`t work , doing it programmatically
        item.setIcon(if (isMenuItemChecked) R.drawable.ic_check_black_24dp else R.drawable.ic_edit_black_24dp)
    }

    private fun updateContact() {
        val contactObj = NewContactRequest()
        contactObj.email = emailInputLayout.editText?.text.toString()
        contactObj.phone = phoneInputLayout.editText?.text.toString()
        contactObj.firstName = nameInputLayout.editText?.text.toString()
        contactObj.lastName = surnameInputLayout.editText?.text.toString()
        contactObj.notes = notesInputLayout.editText?.text.toString()
//        contactObj.images = mContact?.images ?: listOf("Dummy")
        viewModel.updateContact(contactId, contactObj).observe(this, Observer {
            EventBus.getDefault().post(UpdateContacts(true))
        })


    }


    private fun checkRequiredFieldsNotEmpty(): Boolean {

        if (checkInput(nameInputLayout)
            && checkInput(surnameInputLayout)
            && checkInput(phoneInputLayout)
            && checkEmailValidity(emailInputLayout)
            && checkInput(notesInputLayout)
        ) {
            return true
        }
        return false
    }

    private fun checkInput(inputView: TextInputLayout): Boolean {
        if (inputView.editText?.text.isNullOrEmpty()) {
            inputView.isErrorEnabled = true
            inputView.error = getString(R.string.errorText)
            return false
        }
        return true
    }

    private fun checkEmailValidity(inputView: TextInputLayout): Boolean {
        val text = inputView.editText?.text.toString()
        val isValid = (!TextUtils.isEmpty(text) && Patterns.EMAIL_ADDRESS.matcher(text).matches())

        if (!isValid) {
            inputView.isErrorEnabled = true
            inputView.error = getString(R.string.emailErrorText)
        }
        return isValid
    }

    private fun setTextCheckers() {
        nameInputLayout.addTextChecker()
        surnameInputLayout.addTextChecker()
        phoneInputLayout.addTextChecker()
        emailInputLayout.addTextChecker()
        notesInputLayout.addTextChecker()
    }

    private fun updateUi(contact: Contact) {
        nameInputLayout.editText?.setText(contact.firstName)
        surnameInputLayout.editText?.setText(contact.lastName)
        phoneInputLayout.editText?.setText(contact.phone)
        emailInputLayout.editText?.setText(contact.email)
        notesInputLayout.editText?.setText(contact.notes)
        var imageID = getImageID(contact)
        if (!imageID.isNullOrEmpty()) {
            if (!imageID.contains("http")) {
                imageID = MEDIA_PATH.plus(imageID)
            }
            userImage.setImageURI(imageID)
        }
    }

    private fun enableInputLayouts(enable: Boolean) {
        nameInputLayout.editText?.isEnabled = enable
        surnameInputLayout?.isEnabled = enable
        phoneInputLayout?.isEnabled = enable
        emailInputLayout?.isEnabled = enable
        notesInputLayout?.isEnabled = enable
    }

    private fun getImageID(contact: Contact): String? {
        var imageID: String? = null;
        contact.images?.let {
            if (it.isNotEmpty()) {
                imageID = it[0]
            }
        }
        return imageID
    }


    private fun initToolBar() {
        val activity = activity as AppCompatActivity
        activity.setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.contact_details)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener {
            activity.onBackPressed()
        }
        // for showing Back Button
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }
}
