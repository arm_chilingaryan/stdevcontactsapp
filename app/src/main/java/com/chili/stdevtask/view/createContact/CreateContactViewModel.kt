package com.chili.stdevtask.view.createContact

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chili.stdevtask.data.Repository
import com.chili.stdevtask.extensions.showToast
import com.chili.stdevtask.model.NewContactRequest
import com.chili.stdevtask.model.response.Contact
import io.reactivex.disposables.CompositeDisposable
import java.io.File

class CreateContactViewModel(var repository: Repository) : ViewModel() {
    var compositeDisposable = CompositeDisposable()
    var contactLiveData = MutableLiveData<Contact>()


    fun createContact(contactObj: NewContactRequest) {
        createContact(contactObj, null)
    }

    fun createContact(contactObj: NewContactRequest, file: File?): LiveData<Contact> {
        val disposable = repository.createContact(contactObj, file)
                .subscribe({
                    contactLiveData.postValue(it)
                }, {
                    it.printStackTrace()
                    it.message?.let {
                        if (it.contains("Write error")){
                            showToast("File is too large")
                            // create only contact if photo not uploaded
                            createContact(contactObj)
                        }
                    }
                })
        compositeDisposable.add(disposable)
        return contactLiveData
    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed)
            compositeDisposable.dispose()
        super.onCleared()
    }

    class CreateContactViewModelFactory(var repository: Repository) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return CreateContactViewModel(repository) as T
        }
    }
}

