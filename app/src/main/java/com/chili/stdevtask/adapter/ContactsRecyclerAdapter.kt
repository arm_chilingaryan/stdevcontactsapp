package com.chili.stdevtask.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chili.stdevtask.R
import com.chili.stdevtask.data.remote.ContactsApi
import com.chili.stdevtask.data.remote.ContactsApi.Companion.MEDIA_PATH
import com.chili.stdevtask.data.remote.MediaApi.Companion.BASE_URL_MEDIA
import com.chili.stdevtask.model.response.Contact
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.item_contact_rv.view.*

class ContactsRecyclerAdapter(val items: ArrayList<Contact>) : RecyclerView.Adapter<ContactViewHolder>() {
    private var mContactClickListener: ContactClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_contact_rv, parent, false))
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact = getItem(position)
        var imageID = getImageID(contact)
        if (!imageID.isNullOrEmpty()) {
            if (!imageID.contains("http")) {
                imageID = MEDIA_PATH.plus(imageID)
            }
            holder.userImage.setImageURI(imageID)
        } else{
            holder.userImage.setActualImageResource(R.drawable.user_placeholder)
        }

        holder.userNameTV.text = contact.getFullName()
        holder.userPhoneNumberTV.text = contact.phone

        holder.itemView.setOnClickListener {
            mContactClickListener?.onContactClicked(contact = contact, position = position)
        }
    }

    fun getItem(position: Int): Contact {
        return items[position]
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateList(it: List<Contact>?) {
        if (it != null) {
            items.clear()
            items.addAll(it)
            notifyDataSetChanged()
        }
    }


    fun setItemClickListener(contactClickListener: ContactClickListener) {
        mContactClickListener = contactClickListener

    }
}

private fun getImageID(contact: Contact): String? {
    var imageID: String? = null;
    contact.images?.let {
        if (it.isNotEmpty()) {
            imageID = it[0]
        }
    }
    return imageID
}

interface ContactClickListener {
    fun onContactClicked(contact: Contact, position: Int)
}

class ContactViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val userImage: SimpleDraweeView = view.user_image
    val userNameTV: TextView = view.name_tv
    val userPhoneNumberTV: TextView = view.phone_tv

}